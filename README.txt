The UI module provides a Drupal interface to the UI jQuery library.  
It is a successor of the jQuery interface module,
http://drupal.org/project/jquery_interface
The UI module provides access to all UI components, as well as
defining several form elements (like slider).

To add UI javascript, just call
ui_add_js("droppables");
UI will take care of the rest for you.

